// Capture
//
// Written by Richard L Rosenheim
//            richard@rosenheims.com
//            May 21, 2005
//
// Based in a large part upon the DxScan and DxPlayer samples by David Wohlferd
//
// Portions Copyright 2005 - Richard L Rosenheim

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Forms;

using DirectShowLib;


namespace WindowsFormsApplication1
{
    /// <summary> Summary description for MainForm. </summary>
    internal class Capture : ISampleGrabberCB, IDisposable
    {
        #region Member variables

        /// <summary> graph builder interface. </summary>
        private IFilterGraph2 m_FilterGraph;

        /// <summary> Dimensions of the image, calculated once in constructor. </summary>
        private int m_videoWidth;
        private int m_videoHeight;
        private int m_stride;

        private string m_String;
        private Bitmap bitmapOverlay;
        private Font fontOverlay;

#if DEBUG
        // Allow you to "Connect to remote graph" from GraphEdit
        DsROTEntry m_rot = null;
#endif

        #endregion

        #region Our variables
        private Brush fontColor;
        private IVideoWindow mainVideoWindow;
        ISampleGrabber m_sampGrabber = null;
        private Control hWin;
        private bool fullScreen;
        private WindowStyleEx defStyles;
        #endregion

        #region API

        [DllImport("Kernel32.dll", EntryPoint="RtlMoveMemory")]
        private static extern void CopyMemory(IntPtr Destination, IntPtr Source, [MarshalAs(UnmanagedType.U4)] uint Length);

        #endregion

        #region Declarations
        [ComVisible(false), ComImport,
            Guid("e436ebb5-524f-11ce-9f53-0020af0ba770")]
            public class AsyncReader
        {
        }

        // Declare the enum w/o using uint, so it can
        // be used in vb
        [Flags]
            enum WindowStyleFlags
        {
            OVERLAPPED      = 0x00000000,
            POPUP           = -2147483648,
            CHILD           = 0x40000000,
            MINIMIZE        = 0x20000000,
            VISIBLE         = 0x10000000,
            DISABLED        = 0x08000000,
            CLIPSIBLINGS    = 0x04000000,
            CLIPCHILDREN    = 0x02000000,
            MAXIMIZE        = 0x01000000,
            BORDER          = 0x00800000,
            DLGFRAME        = 0x00400000,
            VSCROLL         = 0x00200000,
            HSCROLL         = 0x00100000,
            SYSMENU         = 0x00080000,
            THICKFRAME      = 0x00040000,
            GROUP           = 0x00020000,
            TABSTOP         = 0x00010000,
            MINIMIZEBOX     = 0x00020000,
            MAXIMIZEBOX     = 0x00010000,
        }

        #endregion

        /// <summary> release everything. </summary>
        public void Dispose()
        {
            CloseInterfaces();
            if ( bitmapOverlay != null ) bitmapOverlay.Dispose();
            if ( fontOverlay != null ) fontOverlay.Dispose();
        }
        ~Capture()
        {
            Dispose();
        }

        public void setText(String txt)
        {
            m_String = txt;
        }

        public void setColor(Brush c)
        {
            fontColor = c;
        }

        public void put_CurrentPosition(double pos)
        {
            IMediaPosition mediaPos = m_FilterGraph as IMediaPosition;
            if (mediaPos != null) mediaPos.put_CurrentPosition(pos);
        }

        public void get_CurrentPosition(out double pos)
        {
            IMediaPosition mediaPos = m_FilterGraph as IMediaPosition;
            if (mediaPos != null) mediaPos.get_CurrentPosition(out pos);
            else pos = 0;
        }

        public void put_Volume(int vol)
        {
            IBasicAudio audio = m_FilterGraph as IBasicAudio;
            if (audio != null)
            {
                audio.put_Balance(0);
                audio.put_Volume(vol);
            }
        }

        public void get_Volume(out int vol)
        {
            IBasicAudio audio = m_FilterGraph as IBasicAudio;
            if (audio != null) audio.get_Volume(out vol);
            else vol = 0;
        }

        public void pause()
        {
            IMediaControl mediaCtrl = m_FilterGraph as IMediaControl;
            if (mediaCtrl != null) mediaCtrl.Pause();
        }

        public void get_Duration(out double dur)
        {
            IMediaPosition mediaPos = m_FilterGraph as IMediaPosition;
            if (mediaPos != null) mediaPos.get_Duration(out dur);
            else dur = 0;
        }

        public void SetWindowPosition(int left, int top, int width, int height)
        {
            IVideoWindow videoWindow = m_FilterGraph as IVideoWindow;
            if (videoWindow != null) videoWindow.SetWindowPosition(left, top, width, height);
        }

        /// <summary> Use capture device zero, default frame rate and size</summary>
        public Capture(string FileName, Control hWin)
        {
            _Capture(FileName, hWin);
        }
        /// <summary> capture the next image </summary>
        public void Start()
        {
            IMediaControl mediaCtrl = m_FilterGraph as IMediaControl;

            int hr = mediaCtrl.Run();
            DsError.ThrowExceptionForHR( hr );
        }

        public void toggleFullscreen()
        {
            IVideoWindow videoWindow = mainVideoWindow;
            if (videoWindow != null)
            {
                /*
                 * Using put_fullscreen is deprecated, so we are going to simulate it.
                 */
                if (!fullScreen)
                {
                    videoWindow.put_Visible(OABool.False);
                    videoWindow.put_Owner(IntPtr.Zero);
                    videoWindow.get_WindowStyleEx(out defStyles);
                    videoWindow.put_WindowStyleEx(WindowStyleEx.Topmost);
                    videoWindow.put_WindowStyle(WindowStyle.Popup);
                    videoWindow.SetWindowPosition(0, 0, Screen.GetBounds(hWin).Width, Screen.GetBounds(hWin).Height);
                }
                else
                {
                    videoWindow.put_Owner(IntPtr.Zero);
                    videoWindow.put_WindowStyleEx(defStyles);
                    ConfigureVideoWindow(videoWindow, hWin);
                }
                fullScreen = !fullScreen;
            }
        }

        /// <summary>
        ///  Returns an interface to the event notification interface
        /// </summary>
        public IMediaEventEx MediaEventEx
        {
            get
            {
                return (IMediaEventEx) m_FilterGraph;
            }
        }
		
		
        // Internal capture
        private void _Capture(string FileName, Control hWin)
        {
            try
            {
                // Set up the capture graph
                SetupGraph( FileName, hWin);
                SetupBitmap();
            }
            catch
            {
                Dispose();
                throw;
            }
        }

        // Build the capture graph for grabber and renderer.</summary>
        // (Control to show video in, Filename to play)
        private void SetupGraph(string FileName, Control hWin)
        {
            int hr;
            IGraphBuilder gb = null;

            fullScreen = false;
            // Get the graphbuilder object
            m_FilterGraph = new FilterGraph() as IFilterGraph2;
            gb = m_FilterGraph as IGraphBuilder;

            // Get a ICaptureGraphBuilder2 to help build the graph
            ICaptureGraphBuilder2 icgb2 = new CaptureGraphBuilder2() as ICaptureGraphBuilder2;

            try
            {
                // Link the ICaptureGraphBuilder2 to the IFilterGraph2
                hr = icgb2.SetFiltergraph(gb);
                DsError.ThrowExceptionForHR(hr);

#if DEBUG
                // Allows you to view the graph with GraphEdit File/Connect
                m_rot = new DsROTEntry(m_FilterGraph);
#endif
                // Add the filters necessary to render the file.  This function will
                // work with a number of different file types.
                IBaseFilter sourceFilter = null;
                hr = gb.AddSourceFilter(FileName, FileName, out sourceFilter);
                DsError.ThrowExceptionForHR(hr);

                // Get the SampleGrabber interface
                m_sampGrabber = (ISampleGrabber)new SampleGrabber();
                IBaseFilter baseGrabFlt = (IBaseFilter)m_sampGrabber;

                // Configure the Sample Grabber
                ConfigureSampleGrabber(m_sampGrabber);

                // Add it to the filter
                hr = gb.AddFilter(baseGrabFlt, "Ds.NET Grabber");
                DsError.ThrowExceptionForHR(hr);

                hr = gb.RenderFile(FileName, null);
                DsError.ThrowExceptionForHR(hr);

                IVideoWindow videoWindow;
                // Configure the Video Window
                videoWindow = gb as IVideoWindow;
                ConfigureVideoWindow(videoWindow, hWin);
                mainVideoWindow = videoWindow;

                // Connect the pieces together, use the default renderer
                hr = icgb2.RenderStream(null, null, sourceFilter, baseGrabFlt, null);
                DsError.ThrowExceptionForHR(hr);


                // Now that the graph is built, read the dimensions of the bitmaps we'll be getting
                SaveSizeInfo(m_sampGrabber);

                // Configure the Video Window
                videoWindow = m_FilterGraph as IVideoWindow;
                ConfigureVideoWindow(videoWindow, hWin);
                hr = videoWindow.put_MessageDrain(hWin.FindForm().Handle);
            }
            finally
            {
                if (icgb2 != null)
                {
                    Marshal.ReleaseComObject(icgb2);
                    icgb2 = null;
                }
            }
#if DEBUG
            // Double check to make sure we aren't releasing something
            // important.
            GC.Collect();
            GC.WaitForPendingFinalizers();
#endif
        }

        // Configure the video window
        private void ConfigureVideoWindow(IVideoWindow videoWindow, Control hWin)
        {
            int hr;

            this.hWin = hWin;
            Form1 f = (Form1)hWin.FindForm();
            // Set the output window
            hr = videoWindow.put_Owner(hWin.Handle);
            DsError.ThrowExceptionForHR(hr);
            // Set the window style
            hr = videoWindow.put_WindowStyle((WindowStyle.Child | WindowStyle.ClipChildren | WindowStyle.ClipSiblings));
            DsError.ThrowExceptionForHR(hr);

            // Make the window visible
            hr = videoWindow.put_Visible(OABool.True);
            DsError.ThrowExceptionForHR(hr);

            // Position the playing location
            Rectangle rc = hWin.ClientRectangle;
            hr = videoWindow.SetWindowPosition(0, 0, rc.Right, rc.Bottom);
            DsError.ThrowExceptionForHR(hr);
            int t,l,w, h;
            videoWindow.GetWindowPosition(out t, out l, out w, out h);
            f.Size = new Size(w, h+89);
        }

        /// <summary> Read and store the properties </summary>
        private void SaveSizeInfo(ISampleGrabber sampGrabber)
        {
            int hr;

            // Get the media type from the SampleGrabber
            AMMediaType media = new AMMediaType();
            hr = sampGrabber.GetConnectedMediaType( media );
            DsError.ThrowExceptionForHR( hr );

            if( (media.formatType != FormatType.VideoInfo) || (media.formatPtr == IntPtr.Zero) )
            {
                throw new NotSupportedException( "Unknown Grabber Media Format" );
            }

            // Grab the size info
            VideoInfoHeader videoInfoHeader = (VideoInfoHeader) Marshal.PtrToStructure( media.formatPtr, typeof(VideoInfoHeader) );
            m_videoWidth = videoInfoHeader.BmiHeader.Width;
            m_videoHeight = videoInfoHeader.BmiHeader.Height;
            m_stride = m_videoWidth * (videoInfoHeader.BmiHeader.BitCount / 8);

            DsUtils.FreeAMMediaType(media);
            media = null;
        }
        /// <summary> Set the options on the sample grabber </summary>
        private void ConfigureSampleGrabber(ISampleGrabber sampGrabber)
        {
            AMMediaType media;
            int hr;

            // Set the media type to Video/RBG24
            media = new AMMediaType();
            media.majorType	= MediaType.Video;
            media.subType = MediaSubType.ARGB32;
            media.formatType = FormatType.VideoInfo;

            hr = sampGrabber.SetMediaType( media );
            DsError.ThrowExceptionForHR( hr );

            DsUtils.FreeAMMediaType(media);
            media = null;

            hr = sampGrabber.SetCallback( this, 1 );
            DsError.ThrowExceptionForHR( hr );
        }

        /// <summary> Shut down capture </summary>
        private void CloseInterfaces()
        {
            int hr;

            try
            {
                if( m_FilterGraph != null )
                {
                    IMediaControl mediaCtrl = m_FilterGraph as IMediaControl;

                    // Stop the graph
                    hr = mediaCtrl.Stop();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

#if DEBUG
            if (m_rot != null)
            {
                m_rot.Dispose();
            }
#endif

            if (m_FilterGraph != null)
            {
                Marshal.ReleaseComObject(m_FilterGraph);
                m_FilterGraph = null;
            }
            GC.Collect();
        }

        /// <summary> sample callback, NOT USED. </summary>
        int ISampleGrabberCB.SampleCB( double SampleTime, IMediaSample pSample )
        {
            Marshal.ReleaseComObject(pSample);
            return 0;
        }

        /// <summary> buffer callback, COULD BE FROM FOREIGN THREAD. </summary>
        int ISampleGrabberCB.BufferCB( double SampleTime, IntPtr pBuffer, int BufferLen )
        {
            Graphics g;
            String s;
            float sLeft;
            float sTop;
            SizeF d;
			
            g = Graphics.FromImage(bitmapOverlay);
            g.Clear(System.Drawing.Color.Transparent);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias; 

            // Prepare to put the specified string on the image
            g.DrawRectangle(System.Drawing.Pens.Black, 0, 0, m_videoWidth - 1, m_videoHeight - 1);
            g.DrawRectangle(System.Drawing.Pens.Black, 1, 1, m_videoWidth - 3, m_videoHeight - 3);

            d = g.MeasureString(m_String, fontOverlay);
            while (d.Width > m_videoWidth)
            {
                String[] words = m_String.Split(' ');
                m_String = words[0];
                for (int i = 1; i < words.Length; ++i)
                {
                    if (i == words.Length / 2) m_String += "\n";
                    else m_String += " ";
                    m_String += words[i];
                }
                d = g.MeasureString(m_String, fontOverlay);
            }

            sLeft = (m_videoWidth - d.Width) / 2;
            sTop = (m_videoHeight - d.Height );

            g.DrawString(m_String, fontOverlay, fontColor, 
                sLeft, sTop, System.Drawing.StringFormat.GenericTypographic);
            g.Dispose();

            // need to flip the bitmap so it's the same orientation as the
            // video buffer
            bitmapOverlay.RotateFlip(RotateFlipType.RotateNoneFlipY);
			
            // create and copy the video's buffer image to a bitmap
            Bitmap v;
            v = new Bitmap(m_videoWidth, m_videoHeight, m_stride,
                PixelFormat.Format32bppArgb, pBuffer);
            g = Graphics.FromImage(v);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            // draw the overlay bitmap over the video's bitmap
            g.DrawImage(bitmapOverlay, 0, 0, bitmapOverlay.Width, bitmapOverlay.Height);

            // dispose of the various objects
            g.Dispose();
            v.Dispose();

            return 0;
        }

        void SetupBitmap()
        {
            int fSize;

            fSize = 20;
            bitmapOverlay = new Bitmap(m_videoWidth, m_videoHeight, 
                System.Drawing.Imaging.PixelFormat.Format32bppArgb); 
            fontOverlay = new Font("Times New Roman", fSize, System.Drawing.FontStyle.Bold, 
                System.Drawing.GraphicsUnit.Point);
        }
    }
}
