﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.IO;
using System.Windows.Forms;
namespace WindowsFormsApplication1
{

    class VideoServer
    {
        TcpListener listener;
        Form1 owner;
        Boolean run=true;

        public Boolean isRunning()
        {
            return run;
        }

        public void stop()
        {
            run = false;
        }

        public VideoServer(Form1 owner)
        {
            this.owner = owner;
        }

        public void startServer()
        {
            // Data buffer for incoming data.
            byte[] bytes = new Byte[1024];

            // Bind the socket to the local endpoint and 
            // listen for incoming connections.
            try
            {
                listener = new TcpListener(IPAddress.Any, 4407);
                listener.Start();

                // Start listening for connections.
                while (run)
                {
                    try //We don't want to close socket on a single error
                    {
                        if (!listener.Pending()) //If we don't have a client wanting to connect
                        {
                            Thread.Sleep(500); //wait and
                            continue; // skip to next iteration of loop
                        }
                        TcpClient client = listener.AcceptTcpClient();
                        client.ReceiveTimeout = 10000; // 10 sec
                        Stream handler = client.GetStream();
                        if (client.Connected)
                        {
                            String data = null;
                            //MessageBox.Show("Incoming from " + client.Client.LocalEndPoint.ToString());
                            // An incoming connection needs to be processed.
                            while (run)
                            {
                                bytes = new byte[1024];
                                int bytesRec = handler.Read(bytes, 0, bytes.Length);
                                if (bytesRec > 0)
                                {
                                    data += Encoding.UTF8.GetString(bytes, 0, bytesRec);
                                    if (data.IndexOf('|') > -1)
                                    {
                                        break;
                                    }
                                }
                                else break;
                            }
                                if (data != null && data.Contains("|") )
                                    owner.Invoke((MethodInvoker)delegate //calling setText from UI thread
                                        {
                                            owner.setText(data.Substring(0, data.IndexOf('|')));
                                        });
                                else if (data != null)
                                    owner.Invoke((MethodInvoker)delegate //calling setText from UI thread
                                    {
                                        owner.setText(data);
                                    });
                           handler.Close();
                        }

                    }
                    catch (Exception) {
                        try //Reinitalize socket on error
                        {
                            listener.Stop();
                        }
                        catch (Exception) { }
                        finally
                        {
                            listener = new TcpListener(IPAddress.Any, 4407);
                            listener.Start();
                        }
                    }
                }

            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception e)
            {
                MessageBox.Show(e.StackTrace);
            }
            MessageBox.Show("Socket closed.");
        }
    }
}
