﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Threading;
using DirectShowLib;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private Size oldSize;
        private String sub;
        private Capture cam;
        private IMediaEventEx mediaEvent = null;
        private VideoServer vs;
        private Thread oThread;
        private bool bwFinished;
        private object bwLock = new Object();

        private const int WM_DBLCLICK = 0x0203;
        private const int WM_GRAPHNOTIFY = 0x8000 + 1;

        public Form1()
        {
            InitializeComponent();
            BackColor = Color.Black;
            vs = new VideoServer(this);
            sub = "Ez egy felirat";
            bwFinished = true;
            trackBar1.Value = -2000;

            oThread = new Thread(new ThreadStart(vs.startServer));

            // Start the thread
            oThread.Start();

            bw.RunWorkerCompleted += (sender, args) =>
                {
                  lock(bwLock) {
                      bwFinished=true;
                  }
                };
        }

        private void enableControls()
        {
            controls.Enabled = true;
            controls.Visible = true;
        }

        private void disableControls()
        {
            controls.Enabled = false;
            controls.Visible = false;
        }

        public void setText(String s) {
                sub = s;
                if ( cam != null )
                    cam.setText(sub);
        }

        private void newMovie(String FileName, int call)
        {
            bool bwFinishedLocal;
            lock (bwLock)
            {
                bwFinishedLocal = bwFinished;
            }
            if (!bwFinishedLocal)
            {
                bw.CancelAsync();
                new Thread(new ThreadStart( () => //We have to wait for bw to finish, and we have to wait in a different thread to prevent deadlock.
                {
                    do
                    {
                        lock (bwLock)
                        {
                            bwFinishedLocal = bwFinished;
                        }
                        Thread.Sleep(100);
                    } while (!bwFinishedLocal);
                    this.Invoke((MethodInvoker)delegate
                    {
                        newMovie(FileName,0); //Now we can start a new movie. In UI Thread of course.
                    });
                })).Start();
                return;
            }
            if (cam != null)
            {
                try
                {
                    cam.Dispose();
                }
                catch (Exception e) { } //DontCare
                Thread.Sleep(500); //Let DirectShow to settle down.
            }
            if (System.IO.File.Exists(FileName))
            {
                try
                {
                    cam = new Capture(FileName, videoPanel);
                    cam.setText(sub);
                    cam.setColor(System.Drawing.Brushes.White);
                    trackBar1_ValueChanged(trackBar1, null); //Set volume properly
                    double outval;
                    cam.get_Duration(out outval);
                    pos.Maximum = (int)outval;
                    mediaEvent = cam.MediaEventEx;
                    mediaEvent.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                    enableControls();
                    playToolStripMenuItem_Click(this, null);
                    bwFinished = false;
                    bw.RunWorkerAsync();
                }
                catch (Exception e)
                {
                    if (call < 5)
                    {
                        Thread.Sleep(500);
                        newMovie(FileName, call + 1); //Na mégegyszer
                    }
                    else MessageBox.Show(e.Message.ToString());
                }
            }
            else MessageBox.Show("A megadott fájl nem létezik.");
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog od = new OpenFileDialog();
                od.Title = "Select a Video File";
                if (od.ShowDialog() == DialogResult.OK)
                {
                    newMovie(od.FileName,0);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace.ToString());
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void playToolStripMenuItem_Click(object sender, EventArgs e)
        {
            play.Enabled = false;
            play.Visible = false;
            pause.Enabled = true;
            pause.Visible = true;
            cam.Start();  
        }

        public void Form1_ResizeEnd(object sender, EventArgs e)
        {
            if (cam != null)
            {
                cam.SetWindowPosition(videoPanel.ClientRectangle.Left,
                                videoPanel.ClientRectangle.Top,
                                videoPanel.ClientRectangle.Width,
                                videoPanel.ClientRectangle.Height);
            }
        }

        private void Form1_ResizeBegin(object sender, EventArgs e)
        {
            oldSize = Size;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            bw.CancelAsync();
            vs.stop();
            oThread.Abort();
            Application.Exit();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            playToolStripMenuItem_Click(sender, e);
        }

        private void pause_Click(object sender, EventArgs e)
        {
            play.Enabled = true;
            play.Visible = true;
            pause.Enabled = false;
            pause.Visible = false;
            cam.pause();
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            double ratio = 1;
            if (trackBar1.Value < -3000) ratio = 1.5;
            if (trackBar1.Value == trackBar1.Minimum) ratio = -10000 / trackBar1.Minimum; //-10000 -> No sound
            cam.put_Volume((int)(trackBar1.Value*ratio));
        }

        private void pos_ValueChanged(object sender, EventArgs e)
        {
            cam.put_CurrentPosition(pos.Value);
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            while (!worker.CancellationPending)
            {
                try
                {
                    if (cam != null)
                    {
                        this.Invoke((MethodInvoker)delegate //change value from UI thread
                        {
                            pos.ValueChanged -= pos_ValueChanged;
                            double outval;
                            cam.get_CurrentPosition(out outval);
                            pos.Value = (int)(outval);
                            pos.ValueChanged += pos_ValueChanged;
                        });
                    }
                    Thread.Sleep(500);
                }
                catch (Exception ex) {
                }
            }
            e.Cancel = true;
        }

        private void fullScreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cam != null) cam.toggleFullscreen();
        }

        private void GraphNotifyEvent(ref Message m)
        {
            EventCode eventCode;
            IntPtr p1, p2;
            int hr;
            bool closeCam = false;

            hr = mediaEvent.GetEvent(out eventCode, out p1, out p2, 0);
            while (hr == 0)
            {
                // handle the event
                if ((eventCode == EventCode.ErrorAbort) ||
                    (eventCode == EventCode.Complete) ||
                    (eventCode == EventCode.UserAbort)
                   )
                {
                    Cursor.Current = Cursors.Default;
                    disableControls();
                    closeCam = true;
                }

                // Release parms
                mediaEvent.FreeEventParams(eventCode, p1, p2);

                // check for additional events
                hr = mediaEvent.GetEvent(out eventCode, out p1, out p2, 0);
            }


            if (closeCam) 
            {
            	cam.Dispose();
            	cam = null;
            }
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                // if this is a windows media message
                case WM_GRAPHNOTIFY:
                    GraphNotifyEvent(ref m);
                    break;
                case WM_DBLCLICK:
                    fullScreenToolStripMenuItem_Click(this, null);
                    break;
                // all other messages
                default:
                    // unhandled window message
                    base.WndProc(ref m);
                    break;
            }
        }

    }
}
